package main

import (
	"fmt"
	"io"
	"net/http"

	"github.com/gobwas/ws"
	"github.com/rs/xid"
)

func index(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`<html><head></head><body>
<input type="file" accept="image/*" onchange="preview(event)"><br>
<img id="imgPreview">
<div id="imgGetAct"></div>
<div id="imgShow"></div>
<script>

var ws = new WebSocket('ws://localhost:8080/ws')
var f1l3
function preview(evt) {
	f1l3 = evt.target.files[0]
	console.log('post blob', f1l3)
	ws.send(f1l3) // websocket, post BLOB data type
	var url = URL.createObjectURL(f1l3)
	imgPreview.src = url
	imgPreview.onload = function() {
		URL.revokeObjectURL(url) 
	}
}
ws.onopen = function() {
  console.log('ws opened')
}
ws.onmessage = function(evt) {
	if(typeof evt.data == 'string') {
		imgGetAct.innerHTML += '<button onclick="ws.send(\''+evt.data+'\')">'+f1l3.name+'</button>'
	} else if(evt.data instanceof Blob) {
		var file = evt.data // websocket, get BLOB data type
		console.log('get blob :', file)
		var url = URL.createObjectURL(file)
		var img = document.createElement('img')
		img.src = url
		img.onload = function() {
			URL.revokeObjectURL(url)
		}
		imgShow.appendChild(img)
	}
}	
ws.onclose = function(evt) {
  if (evt.wasClean) {
    alert(` + "`[close] Connection closed cleanly, code=${evt.code} reason=${evt.reason}`" + `);
  } else {
    // e.g. server process killed or network down
    // event.code is usually 1006 in this case
    alert('[close] Connection died');
  }
}
ws.onerror = function(err) {
  alert(` + "`[error] ${err.message}`" + `);
}

</script></body></html>`))
}

type Blob struct {
	header  ws.Header
	payload []byte
}

var blobs = map[string]Blob{}

func wsSrvr(w http.ResponseWriter, r *http.Request) {
	conn, _, _, err := ws.UpgradeHTTP(r, w)
	if err != nil {
		fmt.Println(err)
		w.Write([]byte(""))
		return
	}
	go func() {
		defer conn.Close()
		for {
			header, err := ws.ReadHeader(conn)
			if err != nil {
				// handle error
				fmt.Println("err0", err)
				return
			}
			payload := make([]byte, header.Length)
			_, err = io.ReadFull(conn, payload)
			if err != nil {
				// handle error
				fmt.Println("err1", err)
				return
			}
			if header.Masked {
				ws.Cipher(payload, header.Mask, 0)
			}
			switch header.OpCode {
			case ws.OpText:
				if blob, ok := blobs[string(payload)]; ok { // get blob data type by id
					header = blob.header
					payload = blob.payload
				} else {
					payload = nil
				}
			case ws.OpBinary:
				id := xid.New().String() // create id for posted blob data type
				blobs[id] = Blob{header, payload}
				payload = []byte(id)
				header.Length = int64(len(payload))
				header.OpCode = ws.OpText
			}
			if payload != nil {
				// Reset the Masked flag, server frames must not be masked as
				// RFC6455 says.
				header.Masked = false
				if err := ws.WriteHeader(conn, header); err != nil {
					// handle error
					fmt.Println("err2", err)
					return
				}
				if _, err := conn.Write(payload); err != nil {
					// handle error
					fmt.Println("err3", err)
					return
				}
			}

			if header.OpCode == ws.OpClose {
				return
			}
		}
	}()
}

func main() {
	http.HandleFunc("/", index)
	http.HandleFunc("/ws", wsSrvr)
	http.ListenAndServe(":8080", nil)
}
